<?php
require_once "logica/Usuario.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$usuario= new Usuario();
$usuarios = $usuario -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Usuario</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "apellido" => "<b>Apellido</b>",
    "correo" => "<b>Correo</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$i = 0;
    foreach ($usuarios as $usuarioActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $usuarioActual-> getNombre();
        $datos[$i]["apellido"] = $usuarioActual-> getApellido();
        $datos[$i]["correo"] = $usuarioActual-> getCorreo();
        $datos[$i]["estado"] = (($usuarioActual -> getEstado()==0)?"Deshabilitado":(($usuarioActual -> getEstado()==1)?"Habilitado":"Inactivo"));
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Usuarios", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>