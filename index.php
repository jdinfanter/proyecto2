<?php
session_start();
require_once "logica/Administrador.php";
require_once "logica/Articulista.php";
require_once "logica/Revisor.php";
require_once "logica/Coordinador.php";
require_once "logica/Usuario.php";
require_once "logica/Articulo.php";
require_once "logica/Articulista_Fecha.php";
require_once "logica/Articulo_Fecha.php";
require_once "logica/Usuario_Fecha.php";
require_once "logica/Revisor_Fecha.php";
require_once "logica/Coordinador_Fecha.php";
require_once "logica/Articulo_Articulista.php";
require_once "logica/Articulo_Revisor.php";
require_once "logica/Articulo_Usuario.php";
require_once "logica/Log.php";
date_default_timezone_set('America/Bogota');
$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
} else {
	$_SESSION["id"] = "";
	$_SESSION["rol"] = "";
}
if (isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])) {
	$_SESSION["id"] = "";
}
?>
<html>

<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>

<body>
	<?php 
	$paginasSinSesion = array(
        "presentacion/Autenticar.php",
	    "presentacion/Registro.php",
	);	
	if(in_array($pid, $paginasSinSesion)){
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "presentacion/MenuAdministrador.php";
	    }else if($_SESSION["rol"] == "Articulista"){
	       include "presentacion/MenuArticulista.php";
	    }else if($_SESSION["rol"] == "Revisor"){
	       include "presentacion/MenuRevisor.php";
	    }else if($_SESSION["rol"] == "Coordinador"){
	        include "presentacion/MenuCoordinador.php";
	    }else if($_SESSION["rol"] == "Usuario"){
	        include "presentacion/MenUsuario.php";
	    }
	    include $pid;
	}else{
	    include "presentacion/Login.php";
	}
	?>	
</body>

</html>