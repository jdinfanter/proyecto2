<?php
class  CoordinadorDAO{
    private $idCoordinador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function CoordinadorDAO($idCoordinador = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado=""){
        $this -> idCoordinador = $idCoordinador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }
    
    public function existeCorreo(){
        return "select correo
                from Coordinador
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into Coordinador (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }
    

    public function autenticar(){
        return "select idCoordinador, nombre, apellido, estado
                from Coordinador
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave, foto, estado
                from Coordinador
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }
    
    public function consultarTodos()
    {
        return "select idCoordinador, nombre, apellido, correo, estado
                from Coordinador";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select  idCoordinador , nombre, apellido, correo, estado
                from Coordinador 
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count( idCoordinador )
                from Coordinador";
    }
    
    public function editar(){
        return "update Coordinador
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', estado = '" . $this -> estado . "',foto = '" . $this -> foto . "'
                where idCoordinador = '" . $this -> idCoordinador.  "'";
    }
    
    public function editarFoto(){
        return "update Coordinador
                set foto = '" . $this -> foto . "'
                where idCoordinador = '" . $this -> idCoordinador.  "'";
    }
    public function editar_clave(){
        return "update Coordinador
                set clave = '" . md5($this -> clave) . "'
                where idCoordinador = '" . $this -> idCoordinador.  "'";
    }
    
    public function cambiarEstado($estado){
        return "update Coordinador
                set estado = '" . $estado . "'
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }
    
}

?>