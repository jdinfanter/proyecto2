<?php
class Articulo_RevisorDAO{
    private $idRevisor;
    private $idArticulo; 
    private $observacion;
    private $revisor; 
    private $fecha; 

    public function Articulo_RevisorDAO($idRevisor = "",$idArticulo="", $observacion="",$revisor="",$fecha="" ){
        $this -> idRevisor = $idRevisor;
        $this -> idArticulo= $idArticulo;   
        $this -> observacion = $observacion;
        $this -> revisor = $revisor;
        $this -> fecha = $fecha;
    }

    public function registrar(){
        return "insert into Articulo_Revisor(idRevisor ,idArticulo, observacion,fecha)
                values ('" . $this ->idRevisor. "','" . $this -> idArticulo. "','" . $this -> observacion. "','" . $this -> fecha. "')";
    }
 
    public function actualizar(){
        return "update Articulo_Revisor
                set observacion='".$this -> observacion."' , fecha= '" . $this -> fecha . "'
                where idRevisor= '".$this -> idRevisor."' and idArticulo='".$this -> idArticulo."'";
    }
    
    public function consultar(){
        return "select idRevisor ,idArticulo, observacion
                from Articulo_Revisor
                where idRevisor= '".$this -> idRevisor."' and idArticulo='".$this -> idArticulo."'";
    }
    
    public function consultarRevisor(){
        return "select ar.idRevisor ,ar.idArticulo, ar.observacion,r.nombre,r.apellido,ar.fecha
                from Revisor r, Articulo a, Articulo_Revisor ar
                where r.idRevisor=ar.idRevisor and a.idArticulo = ar.idArticulo and a.idArticulo='".$this -> idArticulo."'";
    }
}

