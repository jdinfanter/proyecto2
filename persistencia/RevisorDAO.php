<?php
class RevisorDAO{
    private $idRevisor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function RevisorDAO($idRevisor= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idRevisor = $idRevisor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from Revisor
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into Revisor (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }
    
    
    public function autenticar(){
        return "select idRevisor, estado, nombre, apellido
                from Revisor
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave, foto, estado
                from Revisor
                where idRevisor = '" . $this -> idRevisor .  "'";
    }
    
    public function consultarTodos()
    {
        return "select idRevisor, nombre, apellido, correo, estado
                from Revisor";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idRevisor, nombre, apellido, correo, estado
                from Revisor
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idRevisor)
                from Revisor";
    }
    
    public function editar(){
        return "update Revisor
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', foto= '" . $this -> foto . "', estado = '" . $this -> estado . "'
                where idRevisor = '" . $this -> idRevisor .  "'";
    }
    
    public function editar_clave(){
        return "update Revisor
                set clave = '" . md5($this -> clave) . "'
                where idRevisor = '" . $this -> idRevisor.  "'";
    }

    public function cambiarEstado($estado){
        return "update Revisor
                set estado = '" . $estado . "'
                where idRevisor = '" . $this -> idRevisor .  "'";
    }
    
}

?>