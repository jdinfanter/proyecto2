<?php
class ArticuloDAO{
    private $idArticulo;
    private $nombre;
    private $descripcion;
    private $estado;
    private $fecha;
    private $articulo;

    public function ArticuloDAO($idArticulo = "", $nombre = "", $descripcion = "", $estado="",$fecha="", $articulo= ""){
        $this -> idArticulo = $idArticulo;
        $this -> nombre = $nombre;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> fecha = $fecha;
        $this -> articulo = $articulo;
    }

   
        
    public function registrar(){
        return "insert into Articulo (nombre, descripcion, estado,fecha,articulo)
                values ('" . $this -> nombre . "','" . $this -> descripcion. "', '" . $this -> estado . "', '" . $this -> fecha . "','" . $this -> articulo . "')";
    }
 
 
    

    public function consultar(){
        return "select idArticulo,nombre, descripcion, estado ,fecha, articulo
                from Articulo
                where idArticulo = '" . $this -> idArticulo.  "'";
    }
    
    public function consultarID(){
        return "select idArticulo
                from Articulo
                where nombre = '" . $this -> nombre.  "' and descripcion ='" .$this->descripcion . "' and estado= '" . $this -> estado . "' and fecha = '" . $this -> fecha."' and articulo = '" . $this -> articulo . "'";
    }
    
    public function consultarTodos()
    {
        return "select idArticulo, nombre, descripcion, estado, fecha, articulo
                from Articulo";
    }
    
    
    public function consultarSInRevisar(){
        return "select idArticulo, nombre, descripcion, estado, fecha, articulo
                from Articulo
                where estado=0";
    }
    
    public function consultar_Autor($idArticulista,$cantidad,$pagina){
        return "select ar.*
                from Articulista a, Articulo ar, Articulo_Articulista aa
                where a.idArticulista = aa.idArticulista and ar.idArticulo = aa.idArticulo and a.idArticulista='" . $idArticulista.  "'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function  consultar_Revisor($id,$cantidad, $pagina){
        return "select a.*
                from Revisor r, Articulo a, Articulo_Revisor ar
                where r.idRevisor= ar.idRevisor and a.idArticulo = ar.idArticulo and r.idRevisor='" . $id.  "'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultar_ArticulosArticulista($idArticulista)
    {   return "select ar.*
                from Articulista a, Articulo ar, Articulo_Articulista aa
                where a.idArticulista = aa.idArticulista and ar.idArticulo = aa.idArticulo and a.idArticulista='" . $idArticulista.  "'";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idArticulo, nombre, descripcion, estado, fecha, articulo
                from Articulo
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarArticulosAprobados($cantidad, $pagina){
        return "select idArticulo, nombre, descripcion, estado, fecha, articulo
                from Articulo
                where estado=2
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idArticulo)
                from Articulo";
    }
    
    public function consultarCantidadArticulos($id){
        return "select count(idArticulista)
                from Articulo_Articulista
                where idArticulista=".$id;
    }
    
    public function consultarCantidadAprobados(){
        return "select count(idArticulo)
                from Articulo
                where estado=2";
    }
    
    public function consultarCantidadArticulosRevisor($id){
        return "select count(idRevisor)
                from Articulo_Revisor
                where idRevisor=".$id;
    }
    
    
    public function consultarFiltro($filtro){
        return "select idArticulo, nombre,descripcion,estado,fecha, articulo
                from Articulo
                where nombre like '%" . $filtro . "%' and estado=2";
    }
    
    
    public function consultarArticulosFecha(){
        return "select count(fecha) from Articulo where fecha =". $this -> fecha;
    }
    
    public function cambiarEstado($idArticulo,$estado){
        return "update Articulo
                set estado = '" . $estado . "'
                where idArticulo =".$idArticulo;
    }
}

?>