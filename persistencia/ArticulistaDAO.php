<?php
class ArticulistaDAO{
    private $idArticulista;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function ArticulistaDAO($idArticulista = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idArticulista = $idArticulista;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from Articulista
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar(){
        return "insert into Articulista (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }
 
 
    public function autenticar(){
        return "select idArticulista, estado, nombre, apellido
                from Articulista
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave,foto, estado
                from Articulista
                where idArticulista = '" . $this -> idArticulista .  "'";
    }
    
    public function consultarTodos()
    {
        return "select idArticulista, nombre, apellido, correo, estado
                from Articulista";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idArticulista, nombre, apellido, correo, estado
                from Articulista
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idArticulista)
                from Articulista";
    }
    
    public function editar(){
        return "update Articulista
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "',  foto = '" . $this -> foto . "', estado = '" . $this -> estado . "' 
                where idArticulista = '" . $this -> idArticulista .  "'";
    }
    
    public function editar_clave(){
        return "update Articulista
                set clave = '" . md5($this -> clave) . "'
                where idArticulista = '" . $this -> idArticulista .  "'";
    }

    public function cambiarEstado($estado){
        return "update Articulista
                set estado = '" . $estado . "'
                where idArticulista = '" . $this -> idArticulista .  "'";
    }
    
}

?>