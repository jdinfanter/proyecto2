<?php
class UsuarioDAO{
    private $idUsuario;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function UsuarioDAO($idUsuario= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado=""){
        $this -> idUsuario = $idUsuario;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }
    
    public function existeCorreo(){
        return "select correo
                from Usuario
                where correo = '" . $this -> correo .  "'";
    }

    public function autenticar(){
        return "select idUsuario , nombre, apellido, estado
                from Usuario
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }
    
    public function registrar(){
        return "insert into Usuario (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave, foto,estado
                from Usuario 
                where idUsuario  = '" . $this -> idUsuario  .  "'";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select  idUsuario , nombre, apellido, correo, estado
                from Usuario
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idUsuario)
                from Usuario";
    }
    
    public function consultarTodos(){
        return "select idUsuario , nombre, apellido, correo, estado
                from Usuario";
    }
    
    public function editar(){
        return "update Usuario 
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', estado = '" . $this -> estado. "', foto = '" . $this -> foto . "'
                where idUsuario  = '" . $this -> idUsuario .  "'";
    }
    
    public function editarFoto(){
        return "update Usuario 
                set foto = '" . $this -> foto . "'
                where idUsuario  = '" . $this -> idUsuario .  "'";
    }
    public function editar_clave(){
        return "update Usuario 
                set clave = '" . md5($this -> clave) . "'
                where idUsuario  = '" . $this ->idUsuario .  "'";
    }
    
    public function cambiarEstado($estado){
        return "update Usuario
                set estado = '" . $estado . "'
                where idUsuario = '" . $this -> idUsuario .  "'";
    }
    
}

?>