<?php
class Articulo_ArticulistaDAO{
    private $idArticulista;
    private $idArticulo; 
    private $nombre;
    private $descripcion;
    private $estado;
    private $autor;
    private $articulo;
    private $fecha;

    public function Articulo_ArticulistaDAO($idArticulista = "",$idArticulo="",$nombre="",$descripcion="",$autor="",$estado="",$articulo="",$fecha="" ){
        $this -> idArticulista = $idArticulista;
        $this -> idArticulo= $idArticulo;
        $this -> nombre = $nombre;
        $this -> descripcion = $descripcion;
        $this -> autor = $autor;
        $this -> estado = $estado;
        $this -> articulo = $articulo;
        $this -> fecha= $fecha;
    }

    public function registrar(){
        return "insert into Articulo_Articulista(idArticulista ,idArticulo)
                values ('" . $this ->idArticulista . "','" . $this -> idArticulo. "')";
    }
    
    public function consultarTodos(){
        return "select ar.idArticulo,ar.nombre, ar.descripcion,a.nombre,a.apellido, ar.estado,ar.articulo,ar.fecha
                from Articulista a, Articulo ar, Articulo_Articulista aa
                where a.idArticulista = aa.idArticulista and ar.idArticulo = aa.idArticulo";
    }
 
    public function consultarSinRevisar($cantidad, $pagina)
    {
        return "select ar.idArticulo,ar.nombre, ar.descripcion,a.nombre,a.apellido, ar.estado,ar.articulo,ar.fecha
                from Articulista a, Articulo ar, Articulo_Articulista aa
                where a.idArticulista = aa.idArticulista and ar.idArticulo = aa.idArticulo and ar.estado=0
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        
    }
    
    public function consultarCantidad(){
        return "select count(estado)
                from Articulo
                where estado=0";
    }
}

