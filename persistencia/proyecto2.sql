-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2020 a las 19:41:07
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Daniel', 'Infante', '123@123.com', '202cb962ac59075b964b07152d234b70', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulista`
--

CREATE TABLE `articulista` (
  `idArticulista` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulista`
--

INSERT INTO `articulista` (`idArticulista`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Pedro', 'Jimenez', '111@111.com', '698d51a19d8a121ce581499d7b701668', NULL, 1),
(2, 'Javier', 'Castro', '112@112.com', '7f6ffaa6bb0b408017b62254211691b5', NULL, 1),
(3, 'Juan', 'Jimenez', '113@113.com', '73278a4a86960eeb576a8fd4c9ec6997', NULL, 1),
(4, 'Andrea', 'Pertuz', '114@114.com', '5fd0b37cd7dbbb00f97ba6ce92bf5add', NULL, 1),
(5, 'Andres', 'Vega', '115@115.com', '2b44928ae11fb9384c4cf38708677c48', NULL, 1),
(6, 'Pepito', 'Vega', '116@116.com', 'c45147dee729311ef5b5c3003946c48f', NULL, 1),
(7, 'Carlos', 'Contreras', '117@117.com', 'eb160de1de89d9058fcb0b968dbbbd68', '', 1),
(8, 'Ricardo', 'Gutierrez', '118@118.com', '5ef059938ba799aaa845e1c2e8a762bd', '', 1),
(9, 'Camila', 'Gutierrez', '119@119.com', '07e1cd7dca89a1678042477183b7ac3f', '', 1),
(10, 'Laura', 'Cardenas', '110@110.com', '5f93f983524def3dca464469d2cf9f3e', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulista_dia`
--

CREATE TABLE `articulista_dia` (
  `id` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `articulista_dia`
--

INSERT INTO `articulista_dia` (`id`, `fecha`, `cantidad`) VALUES
(1, '2020-08-16', 4),
(2, '2020-08-17', 3),
(3, '2020-08-18', 1),
(4, '2020-08-19', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `idArticulo` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `articulo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idArticulo`, `nombre`, `descripcion`, `estado`, `fecha`, `articulo`) VALUES
(1, 'Capitalismo en Europa', 'Articulo sobre capitalismo en Europa Medieval', 2, '2020-08-18', 'Articulos_subidos/1598453827.pdf'),
(2, 'Rasgos de la ciencia', 'Articulo sobre los rasgos genericos y especificos de la ciencia ', 2, '2020-08-18', 'Articulos_subidos/1598455087.pdf'),
(3, 'saber pedagógico y valores morales', 'Articulo sobre el saber pedagógico y valores morales', 2, '2020-08-19', 'Articulos_subidos/1598459150.pdf'),
(4, 'Arqueologia para america latina', 'Articulo sobre Arqueologia para america latina en el siglo XXl', 2, '2020-08-18', 'Articulos_subidos/1598459863.pdf'),
(6, 'gramatica del odio en el capitalismo', 'Articulo sobre la gramatica del odio en el capitalismo contenporaneo', 0, '2020-08-26', 'Articulos_subidos/1598460583.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_articulista`
--

CREATE TABLE `articulo_articulista` (
  `idArticulista` int(11) NOT NULL,
  `idArticulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo_articulista`
--

INSERT INTO `articulo_articulista` (`idArticulista`, `idArticulo`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 6),
(2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_dia`
--

CREATE TABLE `articulo_dia` (
  `id` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `articulo_dia`
--

INSERT INTO `articulo_dia` (`id`, `fecha`, `cantidad`) VALUES
(1, '2020-08-18', 3),
(2, '2020-08-19', 1),
(3, '2020-08-26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_revisor`
--

CREATE TABLE `articulo_revisor` (
  `id` int(11) NOT NULL,
  `idRevisor` int(11) NOT NULL,
  `idArticulo` int(11) NOT NULL,
  `observacion` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo_revisor`
--

INSERT INTO `articulo_revisor` (`id`, `idRevisor`, `idArticulo`, `observacion`, `fecha`) VALUES
(1, 1, 1, 'Buen articulo', '2020-08-26'),
(2, 1, 2, 'Articulo interesante', '2020-08-26'),
(3, 2, 3, 'Interesante Articulo', '2020-08-26'),
(4, 2, 3, 'Buen articulo', '2020-08-26'),
(5, 1, 4, 'Buen Articulo', '2020-08-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_usuario`
--

CREATE TABLE `articulo_usuario` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idArticulo` int(11) NOT NULL,
  `observacion` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo_usuario`
--

INSERT INTO `articulo_usuario` (`id`, `idUsuario`, `idArticulo`, `observacion`, `fecha`) VALUES
(1, 1, 1, 'Buen articulo', '2020-08-26'),
(2, 1, 2, 'Buen articulo', '2020-08-26'),
(3, 2, 3, 'Interesante articulo', '2020-08-26'),
(4, 1, 3, 'Buen articulo', '2020-08-26'),
(5, 1, 4, 'Interesante Articulo sobre Arqueologia', '2020-08-26'),
(6, 2, 4, 'buena', '2020-08-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador`
--

CREATE TABLE `coordinador` (
  `idCoordinador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `coordinador`
--

INSERT INTO `coordinador` (`idCoordinador`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Isabel', 'Reyes', '333@333.com', '310dcbbf4cce62f762a2aaa148d556bd', NULL, 1),
(2, 'Andrea', 'Sissa', '331@331.com', '6da37dd3139aa4d9aa55b8d237ec5d4a', '', 1),
(3, 'Paula', 'Salazar', '332@332.com', 'c042f4db68f23406c6cecf84a7ebb0fe', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_dia`
--

CREATE TABLE `coordinador_dia` (
  `id` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `coordinador_dia`
--

INSERT INTO `coordinador_dia` (`id`, `fecha`, `cantidad`) VALUES
(1, '2020-08-15', 2),
(2, '2020-08-16', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `accion` varchar(300) NOT NULL,
  `datos` varchar(300) NOT NULL,
  `fecha` varchar(30) NOT NULL,
  `hora` varchar(30) NOT NULL,
  `actor` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `accion`, `datos`, `fecha`, `hora`, `actor`) VALUES
(1, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '9:36 am', 'Daniel Infante'),
(2, 'Registro Revisor', 'Nombre: Pedro; Apellido: Rondon; Correo: 221@221.com', '2020-08-26', '9:38 am', 'Pedro Rondon'),
(3, 'Registro Coordinador', 'Nombre: Andrea; Apellido: Sissa; Correo: 331@331.com', '2020-08-26', '9:40 am', 'Andrea Sissa'),
(4, 'Registro Articulista', 'Nombre: Carlos; Apellido: Contreras; Correo: 117@117.com', '2020-08-26', '9:43 am', 'Carlos Contreras'),
(5, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '9:43 am', 'Daniel Infante'),
(6, 'Registro Usuario', 'Nombre: Andres; Apellido: Jimenez; Correo: 441@441.com', '2020-08-26', '9:46 am', 'Andres Jimenez'),
(7, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '9:46 am', 'Daniel Infante'),
(8, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '9:49 am', 'Pedro Jimenez'),
(9, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '9:56 am', 'Daniel Infante'),
(10, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '9:56 am', 'Pedro Jimenez'),
(11, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '9:58 am', 'Daniel Infante'),
(12, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '9:59 am', 'Carolina Castro'),
(13, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '9:59 am', 'Isabel Reyes'),
(14, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '9:59 am', 'Carolina Castro'),
(15, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '10:01 am', 'Ssebastian Jimenez'),
(16, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '10:17 am', 'Pedro Jimenez'),
(17, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '10:18 am', 'Isabel Reyes'),
(18, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '10:18 am', 'Carolina Castro'),
(19, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '10:20 am', 'Ssebastian Jimenez'),
(20, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '10:25 am', 'Daniel Infante'),
(21, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '10:26 am', 'Ssebastian Jimenez'),
(22, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:24 am', 'Daniel Infante'),
(23, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:24 am', 'Daniel Infante'),
(24, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '11:25 am', 'Pedro Jimenez'),
(25, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '11:26 am', 'Carolina Castro'),
(26, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '11:27 am', 'Isabel Reyes'),
(27, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '11:27 am', 'Pedro Rondon'),
(28, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:28 am', 'Daniel Infante'),
(29, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '11:28 am', 'Andres Jimenez'),
(30, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '11:29 am', 'Ssebastian Jimenez'),
(31, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:30 am', 'Daniel Infante'),
(32, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '11:36 am', 'Javier Castro'),
(33, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:37 am', 'Daniel Infante'),
(34, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '11:45 am', 'Pedro Jimenez'),
(35, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '11:48 am', 'Pedro Jimenez'),
(36, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '11:50 am', 'Daniel Infante'),
(37, 'Registro Revisor', 'Nombre: Camila; Apellido: Castellanos; Correo: 223@223.com', '2020-08-26', '11:57 am', 'Camila Castellanos'),
(38, 'Registro Coordinador', 'Nombre: Paula; Apellido: Salazar; Correo: 332@332.com', '2020-08-26', '12:01 pm', 'Paula Salazar'),
(39, 'Registro Usuario', 'Nombre: Daniel; Apellido: Castro; Correo: 443@443.com', '2020-08-26', '12:03 pm', 'Daniel Castro'),
(40, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:03 pm', 'Daniel Infante'),
(41, 'Registro Articulista', 'Nombre: Ricardo; Apellido: Gutierrez; Correo: 118@118.com', '2020-08-26', '12:14 pm', 'Ricardo Gutierrez'),
(42, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:14 pm', 'Daniel Infante'),
(43, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:15 pm', 'Daniel Infante'),
(44, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:15 pm', 'Daniel Infante'),
(45, 'Registro Articulista', 'Nombre: Camila; Apellido: Gutierrez; Correo: 119@119.com', '2020-08-26', '12:16 pm', 'Camila Gutierrez'),
(46, 'Registro Articulista', 'Nombre: Laura; Apellido: Cardenas; Correo: 110@110.com', '2020-08-26', '12:16 pm', 'Laura Cardenas'),
(47, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:16 pm', 'Daniel Infante'),
(48, 'Registro Usuario', 'Nombre: Robinson; Apellido: Mogollon; Correo: 442@442.com', '2020-08-26', '12:27 pm', 'Robinson Mogollon'),
(49, 'Registro Usuario', 'Nombre: Juliana; Apellido: Rivera; Correo: 445@445.com', '2020-08-26', '12:28 pm', 'Juliana Rivera'),
(50, 'Registro Usuario', 'Nombre: Robinson; Apellido: Mogollon; Correo: 447@447.com', '2020-08-26', '12:29 pm', 'Robinson Mogollon'),
(51, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:30 pm', 'Daniel Infante'),
(52, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:31 pm', 'Daniel Infante'),
(53, 'Ingreso al sistema', 'Rol: Articulista', '2020-08-26', '12:34 pm', 'Pedro Jimenez'),
(54, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '12:35 pm', 'Isabel Reyes'),
(55, 'Ingreso al sistema', 'Rol: Revisor', '2020-08-26', '12:36 pm', 'Carolina Castro'),
(56, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '12:37 pm', 'Ssebastian Jimenez'),
(57, 'Ingreso al sistema', 'Rol: Usuario', '2020-08-26', '12:38 pm', 'Andres Jimenez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisor`
--

CREATE TABLE `revisor` (
  `idRevisor` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `revisor`
--

INSERT INTO `revisor` (`idRevisor`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Carolina', 'Castro', '222@222.com', 'bcbe3365e6ac95ea2c0343a2395834dd', NULL, 1),
(2, 'Pedro', 'Rondon', '221@221.com', '060ad92489947d410d897474079c1477', '', 1),
(3, 'Camila', 'Castellanos', '223@223.com', '115f89503138416a242f40fb7d7f338e', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisor_dia`
--

CREATE TABLE `revisor_dia` (
  `id` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `revisor_dia`
--

INSERT INTO `revisor_dia` (`id`, `fecha`, `cantidad`) VALUES
(1, '2020-08-18', 2),
(2, '2020-08-19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Ssebastian', 'Jimenez', '444@444.com', '550a141f12de6341fba65b0ad0433500', NULL, 1),
(2, 'Andres', 'Jimenez', '441@441.com', '15d4e891d784977cacbfcbb00c48f133', '', 1),
(3, 'Daniel', 'Castro', '443@443.com', '13f3cf8c531952d72e5847c4183e6910', '', 1),
(4, 'Robinson', 'Mogollon', '442@442.com', 'c203d8a151612acf12457e4d67635a95', '', 1),
(5, 'Juliana', 'Rivera', '445@445.com', '67f7fb873eaf29526a11a9b7ac33bfac', '', 1),
(6, 'Robinson', 'Mogollon', '447@447.com', '9a96876e2f8f3dc4f3cf45f02c61c0c1', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_dia`
--

CREATE TABLE `usuario_dia` (
  `id` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario_dia`
--

INSERT INTO `usuario_dia` (`id`, `fecha`, `cantidad`) VALUES
(1, '2020-08-18', 2),
(2, '2020-08-19', 1),
(3, '2020-08-20', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `articulista`
--
ALTER TABLE `articulista`
  ADD PRIMARY KEY (`idArticulista`);

--
-- Indices de la tabla `articulista_dia`
--
ALTER TABLE `articulista_dia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idArticulo`);

--
-- Indices de la tabla `articulo_articulista`
--
ALTER TABLE `articulo_articulista`
  ADD PRIMARY KEY (`idArticulista`,`idArticulo`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `articulo_dia`
--
ALTER TABLE `articulo_dia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulo_revisor`
--
ALTER TABLE `articulo_revisor`
  ADD PRIMARY KEY (`id`,`idRevisor`,`idArticulo`),
  ADD KEY `idRevisor` (`idRevisor`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `articulo_usuario`
--
ALTER TABLE `articulo_usuario`
  ADD PRIMARY KEY (`id`,`idUsuario`,`idArticulo`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD PRIMARY KEY (`idCoordinador`);

--
-- Indices de la tabla `coordinador_dia`
--
ALTER TABLE `coordinador_dia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idLog`);

--
-- Indices de la tabla `revisor`
--
ALTER TABLE `revisor`
  ADD PRIMARY KEY (`idRevisor`);

--
-- Indices de la tabla `revisor_dia`
--
ALTER TABLE `revisor_dia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `usuario_dia`
--
ALTER TABLE `usuario_dia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articulista`
--
ALTER TABLE `articulista`
  MODIFY `idArticulista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `articulista_dia`
--
ALTER TABLE `articulista_dia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `articulo_dia`
--
ALTER TABLE `articulo_dia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articulo_revisor`
--
ALTER TABLE `articulo_revisor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `articulo_usuario`
--
ALTER TABLE `articulo_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  MODIFY `idCoordinador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `coordinador_dia`
--
ALTER TABLE `coordinador_dia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `revisor`
--
ALTER TABLE `revisor`
  MODIFY `idRevisor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `revisor_dia`
--
ALTER TABLE `revisor_dia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuario_dia`
--
ALTER TABLE `usuario_dia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo_articulista`
--
ALTER TABLE `articulo_articulista`
  ADD CONSTRAINT `articulo_articulista_ibfk_1` FOREIGN KEY (`idArticulista`) REFERENCES `articulista` (`idArticulista`),
  ADD CONSTRAINT `articulo_articulista_ibfk_2` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);

--
-- Filtros para la tabla `articulo_revisor`
--
ALTER TABLE `articulo_revisor`
  ADD CONSTRAINT `articulo_revisor_ibfk_1` FOREIGN KEY (`idRevisor`) REFERENCES `revisor` (`idRevisor`),
  ADD CONSTRAINT `articulo_revisor_ibfk_2` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);

--
-- Filtros para la tabla `articulo_usuario`
--
ALTER TABLE `articulo_usuario`
  ADD CONSTRAINT `articulo_usuario_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`),
  ADD CONSTRAINT `articulo_usuario_ibfk_2` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
