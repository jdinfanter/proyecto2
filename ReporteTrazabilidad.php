<?php
require_once "logica/Articulo.php";
require_once "logica/Articulo_Revisor.php";
require_once "logica/Articulo_Usuario.php";
require_once "ezpdf/class.ezpdf.php";


$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$idArticulo = $_GET["idArticulo"];
$articulo = new Articulo($idArticulo,"","","","","");
$articulo->consultar();
$articulo_revisor = new Articulo_Revisor("",$idArticulo,"","","");
$articulos_revisores=$articulo_revisor ->consultarRevisor();
$articulo_usuario = new Articulo_Usuario("",$idArticulo,"","","");
$articulos=$articulo_usuario ->consultarUsuario();

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Trazabilidad</b>", 16, $opciones);
$pdf -> selectFont("ezpdf/fonts/Helvetica.afm");
$opciones2 = array("justification" => "left","spacing" =>"2");
$pdf -> ezText("<b>Nombre Articulo: </b>".$articulo ->getNombre(), 12, $opciones2);
$opciones2 = array("justification" => "left","spacing" =>"1.5");
$mensaje="";
if($articulo->getEstado()==0)
{
    $mensaje = "Sin revisar";
}
else if($articulo->getEstado()==1)
{
    $mensaje = "En revision";
}else if($articulo->getEstado()==2)
{
    $mensaje = "Aprobado";
}
else if($articulo->getEstado()==-1)
{
    $mensaje = "Pendiente";
}

$pdf -> ezText("<b>Estado: </b>".$mensaje, 12, $opciones2);
$pdf -> ezText("<b>Descripcion: </b>".$articulo -> getDescripcion(), 12, $opciones2);
$pdf -> ezText("<b>Fecha de subida: </b>".$articulo -> getFecha(), 12, $opciones2);
$revisor="";
$aux2=0;
foreach ($articulos_revisores as $articuloRevisorActual){
    if($articuloRevisorActual->getRevisor()=="")
    {
        $aux2=0;
    }else {
        $aux2=1;
    }
}
if (!$aux2==0)
{
   
    $revisor = $articulos_revisores[0] -> getRevisor();
    $pdf -> ezText("<b>Revisor: </b>".$revisor, 12, $opciones2);
}else{
}

if(!$aux2==0)
{
    if (!$articulos_revisores[0] -> getObservacion()=="")
    {
        $pdf -> ezText("<b>Comentarios de $revisor : </b>", 12, $opciones2);
        foreach ($articulos_revisores as $articuloRevisorActual){
            $pdf -> ezText("<b>-</b>".$articuloRevisorActual ->getObservacion(), 12, $opciones2);
            $opciones3 = array("justification" => "right","spacing" =>"2");
            $pdf -> ezText("<b>comentario realizado la fecha: </b>".$articuloRevisorActual ->getFecha(), 8, $opciones3);
        } 
    }else{
       // $pdf -> ezText("No se registran Comentarios", 12, $opciones2);
    }
}
$aux=0;
foreach ($articulos as $articuloActual){
    if($articulos[0]->getObservacion()=="")
    {
        $aux=0;
    }else {
        $aux=1;
    }
}

if (!$aux==0)
{
    $opciones3 = array("justification" => "center","spacing" =>"2");
    $pdf -> ezText("<b>Comentarios Realiazados por los Usuario</b>", 12, $opciones3);
}

foreach ($articulos as $articuloActual)
{
    $usuario = $articuloActual ->getUsuario();
    $pdf -> ezText("<b>Comentarios de $usuario : </b>", 12, $opciones2);
    $pdf -> ezText("<b></b>".$articuloActual->getObservacion(), 12, $opciones2);
    $opciones3 = array("justification" => "right","spacing" =>"1");
    $pdf -> ezText("<b>comentario realizado la fecha: </b>".$articuloActual ->getFecha(), 8, $opciones3);
}



$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>