<?php
require_once "logica/Coordinador.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$coordinador= new Coordinador();
$coordinadores= $coordinador -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Coordinador</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "apellido" => "<b>Apellido</b>",
    "correo" => "<b>Correo</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$i = 0;
foreach ($coordinadores as $coordinadorActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $coordinadorActual-> getNombre();
        $datos[$i]["apellido"] = $coordinadorActual-> getApellido();
        $datos[$i]["correo"] = $coordinadorActual-> getCorreo();
        $datos[$i]["estado"] = (($coordinadorActual -> getEstado()==0)?"Deshabilitado":(($coordinadorActual -> getEstado()==1)?"Habilitado":"Inactivo"));
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Coordinadores", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>