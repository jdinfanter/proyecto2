<?php
require_once "logica/Articulo_Articulista.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(1, 1, 1, 1);

$articulo = new Articulo_Articulista();
$articulos = $articulo -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Articulos</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "fecha" => "<b>Fecha</b>",
    "autor" => "<b>Autor</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$i = 0;
$mensaje="";
foreach ($articulos as $articuloActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $articuloActual -> getNombre();
        $datos[$i]["fecha"] = $articuloActual-> getFecha();
        $datos[$i]["autor"] = $articuloActual -> getAutor();
        if($articuloActual->getEstado()==0)
        {
            $mensaje = "Sin revisar";
        }
        else if($articuloActual->getEstado()==1)
        {
            $mensaje = "En revision";
        }else if($articuloActual->getEstado()==2)
        {
            $mensaje = "Aprobado";
        }
        else if($articuloActual->getEstado()==-1)
        {
            $mensaje = "Pendiente";
        }
        $datos[$i]["estado"] = $mensaje;
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Articulos", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>