<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogDAO.php";

class Log{
    private $idLog;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;
    private $conexion;
    private $logDAO;
    
    public function Log ($idLog = "", $accion = "", $datos = "", $fecha = "", $hora = "", $actor=""){
        $this->idLog = $idLog;
        $this->accion = $accion;
        $this->datos = $datos;
        $this->fecha = $fecha;
        $this->hora = $hora;
        $this->actor = $actor;
        $this->conexion = new Conexion();
        $this->logDAO = new LogDAO($idLog, $accion, $datos, $fecha, $hora, $actor);
    }
    
    
    public function getIdLog(){
        return $this->idLog;
    }
    
    
    public function getAccion(){
        return $this->accion;
    }
    
    
    public function getDatos(){
        return $this->datos;
    }
    
    
    public function getFecha(){
        return $this->fecha;
    }
    
    
    public function getHora(){
        return $this->hora;
    }
    
    
    public function getActor(){
        return $this->actor;
    }
    
    public function insertar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->logDAO->insertar());
        $this->conexion->cerrar();
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->logDAO->consultarPaginacion($cantidad, $pagina));
        $logs = array();
        while (($resultado = $this->conexion->extraer()) != null) {
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $l);
        }
        $this->conexion->cerrar();
        return $logs;
    }
    
    public function consultarCantidad(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->logDAO->consultarCantidad());
        $this->conexion->cerrar();
        return $this->conexion->extraer()[0];
    }
}
