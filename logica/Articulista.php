<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ArticulistaDAO.php";
class Articulista{
    private $idArticulista;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;      
    private $conexion;
    private $ArticulistaDAO;

    public function getIdArticulista(){
        return $this -> idArticulista;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Articulista($idArticulista = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idArticulista = $idArticulista;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> ArticulistaDAO = new ArticulistaDAO($this -> idArticulista, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> registrar());
        $this -> conexion -> cerrar();          
    }
    
    
    
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idArticulista = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> estado = $resultado[5];
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> consultarPaginacion($cantidad, $pagina));
        $articulistas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulista($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($articulistas, $a);
        }
        $this -> conexion -> cerrar();
        return $articulistas;
        echo $articulistas;
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> consultarTodos());
        $articulistas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulista($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($articulistas, $a);
        }
        $this -> conexion -> cerrar();
        return $articulistas;
    }
    
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticulistaDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
}

?>