<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Revisor_FechaDAO.php";
class Revisor_Fecha{
    private $id;
    private $fecha;
    private $cantidad;
    private $revisor_fechaDAO;

    public function getId(){
        return $this -> id;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function Revisor_Fecha($id= "",$fecha="", $cantidad= ""){
        $this -> id= $id;
        $this -> fecha = $fecha;
        $this -> cantidad= $cantidad;
        $this -> conexion = new Conexion();
        $this -> revisor_fechaDAO = new Revisor_FechaDAO($this -> id,$this -> fecha, $this -> cantidad);
    }
   
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO-> registrar());
        $this -> conexion -> cerrar();          
    }
    public function existeFecha(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO-> existeFecha());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> id= $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> cantidad = $resultado[2];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO ->  consultarTodos());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Revisor_Fecha($resultado[0], $resultado[1], $resultado[2]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarID(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO -> consultarID());
        $resultado = $this -> conexion -> extraer();
        $id = $resultado[0];
        $this -> conexion -> cerrar();
        return $id;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO -> consultarCantidad());
        $resultado = $this -> conexion -> extraer();
        $id = $resultado[0];
        $this -> conexion -> cerrar();
        return $id;
    }
    
    
    public function modificar($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisor_fechaDAO-> modificar($id));
        $this -> conexion -> cerrar();
    }
    
}