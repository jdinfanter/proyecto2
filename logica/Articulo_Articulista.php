<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Articulo_ArticulistaDAO.php";
class Articulo_Articulista{
    private $idArticulista;
    private $idArticulo;
    private $nombre;
    private $descripcion;
    private $estado;
    private $autor;
    private $articulo;
    private $fecha;
    private $conexion;
    private $articulo_articulistaDAO;

    public function getIdArticulista(){
        return $this -> idArticulista;
    }

    public function getIdArticulo(){
        return $this -> idArticulo;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getDescripcion(){
        return $this -> descripcion;
    }
    
    public function getAutor(){
        return $this -> autor;
    }
    
    public function getEstado(){
        return $this -> estado;
    }
    
    public function getArticulo(){
        return $this -> articulo;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    public function Articulo_Articulista($idArticulista = "",$idArticulo="",$nombre="",$descripcion="",$autor="",$estado="",$articulo="",$fecha="" ){
        $this -> idArticulista = $idArticulista;
        $this -> idArticulo= $idArticulo;
        $this -> nombre = $nombre;
        $this -> descripcion = $descripcion;
        $this -> autor = $autor;
        $this -> estado = $estado;
        $this -> articulo= $articulo;
        $this -> fecha= $fecha;
        $this -> conexion = new Conexion();
        $this -> articulo_articulistaDAO = new Articulo_ArticulistaDAO($this -> idArticulista, $this -> idArticulo, $this -> articulo);
    }
   
 
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  articulo_articulistaDAO -> registrar());
        $this -> conexion -> cerrar();          
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_articulistaDAO ->  consultarTodos());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo_Articulista("",$resultado[0],$resultado[1], $resultado[2], $resultado[3]." ".$resultado[4],$resultado[5],$resultado[6],$resultado[7]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarSinRevisar($cantidad, $pagina)
    {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_articulistaDAO  -> consultarSinRevisar($cantidad, $pagina));
        $articulistas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo_Articulista("",$resultado[0],$resultado[1], $resultado[2], $resultado[3]." ".$resultado[4],$resultado[5],$resultado[6],$resultado[7]);
            array_push($articulistas, $a);
        }
        $this -> conexion -> cerrar();
        return $articulistas;
        echo $articulistas;
        
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_articulistaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    
    
}

?>