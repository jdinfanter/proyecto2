<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Articulo_RevisorDAO.php";
class Articulo_Revisor{
    private $idRevisor;
    private $idArticulo;   
    private $observacion;  
    private $revisor; 
    private $fecha; 
    private $conexion;
    private $articulo_revisorDAO;

    public function getIdRevisor(){
        return $this -> idRevisor;
    }

    public function getIdArticulo(){
        return $this -> idArticulo;
    }
    
    public function getObservacion(){
        return $this -> observacion;
    }
    
    public function getRevisor(){
        return $this -> revisor;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    
    public function Articulo_Revisor($idRevisor = "",$idArticulo="", $observacion="",$revisor="",$fecha="" ){
        $this -> idRevisor = $idRevisor;
        $this -> idArticulo= $idArticulo;
        $this -> observacion = $observacion;
        $this -> revisor = $revisor;
        $this -> fecha = $fecha;
        $this -> conexion = new Conexion();
        $this -> articulo_revisorDAO = new Articulo_RevisorDAO($this -> idRevisor, $this -> idArticulo, $this ->observacion, $this->revisor, $this-> fecha);
    }
   

    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  articulo_revisorDAO -> registrar());
        $this -> conexion -> cerrar();          
    }
    
    public function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  articulo_revisorDAO -> actualizar());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_revisorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idRevisor = $resultado[0];
        $this -> idArticulo = $resultado[1];
        $this -> observacion = $resultado[2];
    }
    
   /* public function consultarRevisor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_revisorDAO -> consultarRevisor());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idRevisor = $resultado[0];
        $this -> idArticulo = $resultado[1];
        $this -> observacion = $resultado[2];
        $this -> revisor = $resultado[3]." ".$resultado[4];
        $this -> fecha = $resultado[5];
    }*/
    
    public function consultarRevisor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_revisorDAO -> consultarRevisor());
        $revisores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r= new Articulo_Revisor($resultado[0], $resultado[1], $resultado[2],$resultado[3]." ".$resultado[4],$resultado[5]);
            array_push($revisores, $r);
        }
        $this -> conexion -> cerrar();
        return $revisores;
    }
    
}

?>