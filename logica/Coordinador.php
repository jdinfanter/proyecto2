<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CoordinadorDAO.php";
class Coordinador{
    private $idCoordinador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $coordinadorDAO;

    public function getIdCoordinador(){
        return $this -> idCoordinador;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }
    
    public function getEstado(){
        return $this -> estado;
    }

    public function Coordinador($idCoordinador = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "",$estado=""){
        $this -> idCoordinador = $idCoordinador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> coordinadorDAO = new CoordinadorDAO($this -> idCoordinador , $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto,$this -> estado);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> autenticar());
        $this -> conexion -> cerrar();       
        if ($this -> conexion -> numFilas() == 1){            
            $resultado = $this -> conexion -> extraer();
            $this -> idCoordinador = $resultado[0];  
            $this -> nombre = $resultado[1];  
            $this -> apellido = $resultado[2];    
            $this -> estado = $resultado[3];  
            return true;        
        }else {
            return false;
        }
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave= $resultado[3];
        $this -> foto= $resultado[4];
        $this -> estado= $resultado[5];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> consultarTodos());
        $coordinadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Coordinador($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push( $coordinadores, $c);
        }
        $this -> conexion -> cerrar();
        return $coordinadores;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> consultarPaginacion($cantidad, $pagina));
        $coordinadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Coordinador($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push( $coordinadores, $c);
        }
        $this -> conexion -> cerrar();
        return $coordinadores;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->coordinadorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO -> editar_clave());
        $this -> conexion -> cerrar();
    }
    
    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinadorDAO-> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
}

?>