<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/RevisorDAO.php";

class Revisor{
    private $idRevisor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;      
    private $conexion;
    private $revisorDAO;

    public function getIdRevisor(){
        return $this -> idRevisor;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Revisor($idRevisor= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idRevisor = $idRevisor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> revisorDAO = new RevisorDAO($this -> idRevisor, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idRevisor = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> estado = $resultado[5];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> consultarPaginacion($cantidad, $pagina));
        $revisores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r = new Revisor($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($revisores, $r);
        }
        $this -> conexion -> cerrar();
        return $revisores;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->revisorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> consultarTodos());
        $revisores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r = new Revisor($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($revisores, $r);
        }
        $this -> conexion -> cerrar();
        return $revisores;
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO-> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> revisorDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
}

?>