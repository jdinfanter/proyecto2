<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Articulo_UsuarioDAO.php";
class Articulo_Usuario{
    private $idUsuario;
    private $idArticulo;   
    private $observacion;  
    private $usuario; 
    private $fecha; 
    private $conexion;
    private $articulo_usuarioDAO;

    public function getIdUsuario(){
        return $this -> idUsuario;
    }

    public function getIdArticulo(){
        return $this -> idArticulo;
    }
    
    public function getObservacion(){
        return $this -> observacion;
    }
    
    public function getUsuario(){
        return $this -> usuario;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    public function Articulo_Usuario($idUsuario = "",$idArticulo="", $observacion="",$usuario="",$fecha="" ){
        $this -> idUsuario = $idUsuario ;
        $this -> idArticulo= $idArticulo;
        $this -> observacion = $observacion;
        $this -> usuario = $usuario;
        $this -> fecha = $fecha;
        $this -> conexion = new Conexion();
        $this -> articulo_usuarioDAO = new Articulo_UsuarioDAO($this -> idUsuario, $this -> idArticulo, $this ->observacion, $this->usuario, $this->fecha);
    }
   

    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  articulo_usuarioDAO -> registrar());
        $this -> conexion -> cerrar();          
    }
    
    public function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  articulo_usuarioDAO -> actualizar());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_usuarioDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idRevisor = $resultado[0];
        $this -> idArticulo = $resultado[1];
        $this -> observacion = $resultado[2];
    }
    
    /*public function consultarUsuario(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_usuarioDAO -> consultarUsuario());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idUsuario = $resultado[0];
        $this -> idArticulo = $resultado[1];
        $this -> observacion = $resultado[2];
        $this -> revisor = $resultado[3]." ".$resultado[4];
        $this -> fecha = $resultado[5];
    }*/
    
    public function consultarUsuario(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articulo_usuarioDAO -> consultarUsuario());
        $usuarios = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $u= new Articulo_Usuario($resultado[0], $resultado[1], $resultado[2],$resultado[3]." ".$resultado[4],$resultado[5]);
            array_push($usuarios, $u);
        }
        $this -> conexion -> cerrar();
        return $usuarios;
    }
}

?>