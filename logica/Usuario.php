<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/UsuarioDAO.php";
class Usuario{
    private $idUsuario;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $usuarioDAO;

    public function getIdUsuario(){
        return $this -> idUsuario;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Usuario($idUsuario= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado=""){
        $this -> idUsuario = $idUsuario;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> usuarioDAO = new UsuarioDAO($this -> idUsuario, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO  -> autenticar());
        $this -> conexion -> cerrar();       
        if ($this -> conexion -> numFilas() == 1){            
            $resultado = $this -> conexion -> extraer();
            $this -> idUsuario = $resultado[0];  
            $this -> nombre = $resultado[1];  
            $this -> apellido = $resultado[2];    
            $this -> estado= $resultado[3];
            return true;        
        }else {
            return false;
        }
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO  -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave= $resultado[3];
        $this -> foto= $resultado[4];
        $this -> estado= $resultado[5];
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarPaginacion($cantidad, $pagina));
        $usuarios = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $u = new Usuario($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($usuarios, $u);
        }
        $this -> conexion -> cerrar();
        return $usuarios;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTodos());
        $usuarios = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $u = new Usuario($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($usuarios, $u);
        }
        $this -> conexion -> cerrar();
        return $usuarios;
    }
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO  -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO  -> editar_clave());
        $this -> conexion -> cerrar();
    }
    
    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
    
}

?>