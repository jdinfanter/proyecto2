<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Coordinador_FechaDAO.php";
class Coordinador_Fecha{
    private $id;
    private $fecha;
    private $cantidad;
    private $coordinador_fechaDAO;

    public function getId(){
        return $this -> id;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function Coordinador_Fecha($id= "",$fecha="", $cantidad= ""){
        $this -> id= $id;
        $this -> fecha = $fecha;
        $this -> cantidad= $cantidad;
        $this -> conexion = new Conexion();
        $this -> coordinador_fechaDAO = new Coordinador_FechaDAO($this -> id,$this -> fecha, $this -> cantidad);
    }
   
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO-> registrar());
        $this -> conexion -> cerrar();          
    }
    public function existeFecha(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO-> existeFecha());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> id= $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> cantidad = $resultado[2];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO ->  consultarTodos());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Coordinador_Fecha($resultado[0], $resultado[1], $resultado[2]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarID(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO -> consultarID());
        $resultado = $this -> conexion -> extraer();
        $id = $resultado[0];
        $this -> conexion -> cerrar();
        return $id;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO -> consultarCantidad());
        $resultado = $this -> conexion -> extraer();
        $id = $resultado[0];
        $this -> conexion -> cerrar();
        return $id;
    }
    
    
    public function modificar($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> coordinador_fechaDAO-> modificar($id));
        $this -> conexion -> cerrar();
    }
    
}