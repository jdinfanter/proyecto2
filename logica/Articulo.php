<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ArticuloDAO.php";
class Articulo{
    private $idArticulo;
    private $nombre;
    private $descripcion;
    private $estado;
    private $fecha;
    private $articulo;     
    private $conexion;
    private $articuloDAO;

    public function getIdArticulo(){
        return $this -> idArticulo;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getDescripcion(){
        return $this -> descripcion;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
    
    public function getArticulo(){
        return $this -> articulo;
    }
    
    public function Articulo($idArticulo = "", $nombre = "", $descripcion = "", $estado="",$fecha="", $articulo= ""){
        $this -> idArticulo = $idArticulo;
        $this -> nombre = $nombre;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> fecha = $fecha;
        $this -> articulo = $articulo;
        $this -> conexion = new Conexion();
        $this -> articuloDAO = new ArticuloDAO($this -> idArticulo, $this -> nombre, $this -> descripcion, $this ->estado, $this -> fecha, $this -> articulo);
    }
   
   

  
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> registrar());
        $this -> conexion -> cerrar();          
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idArticulo = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> descripcion = $resultado[2];
        $this -> estado = $resultado[3];
        $this -> fecha = $resultado[4];
        $this -> articulo = $resultado[5];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO ->  consultarTodos());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarSInRevisar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO ->  consultarSInRevisar());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultar_Autor($idArticulista,$cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO ->  consultar_Autor($idArticulista,$cantidad,$pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultar_Revisor($id,$cantidad, $pagina)
    {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO ->  consultar_Revisor($id,$cantidad, $pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultar_ArticulosArticulista($idArticulista){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO ->  consultar_ArticulosArticulista($idArticulista));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarPaginacion($cantidad, $pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarArticulosAprobados($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarArticulosAprobados($cantidad, $pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    
    public function consultarID(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarID());
        $resultado = $this -> conexion -> extraer();
        $id = $resultado[0];
        $this -> conexion -> cerrar();
        return $id;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
        
        
    }
    public function consultarCantidadArticulos($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidadArticulos($id));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarCantidadAprobados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidadAprobados());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarCantidadArticulosRevisor($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidadArticulosRevisor($id));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarFiltro($filtro));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a= new Articulo($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4],$resultado[5]);
            array_push($articulos, $a);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarArticulosFecha()
    {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarArticulosFecha());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
        
    }
    
    public function cambiarEstado($idArticulo,$estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO-> cambiarEstado($idArticulo,$estado));
        $this -> conexion -> cerrar();
    }
    
}

?>