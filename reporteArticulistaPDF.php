<?php
require_once "logica/Articulista.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$articulista = new Articulista();
$articulistas = $articulista -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Articulistas</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "apellido" => "<b>Apellido</b>",
    "correo" => "<b>Correo</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$i = 0;
    foreach ($articulistas as $articulistaActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $articulistaActual -> getNombre();
        $datos[$i]["apellido"] = $articulistaActual-> getApellido();
        $datos[$i]["correo"] = $articulistaActual -> getCorreo();
        $datos[$i]["estado"] = (($articulistaActual -> getEstado()==0)?"Deshabilitado":(($articulistaActual -> getEstado()==1)?"Habilitado":"Inactivo"));
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Articulistas", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>