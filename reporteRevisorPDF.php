<?php
require_once "logica/Revisor.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$revisor= new Revisor();
$revisores = $revisor -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Reporte Revisor</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "apellido" => "<b>Apellido</b>",
    "correo" => "<b>Correo</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$i = 0;
foreach ($revisores as $revisorActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $revisorActual-> getNombre();
        $datos[$i]["apellido"] = $revisorActual -> getApellido();
        $datos[$i]["correo"] = $revisorActual-> getCorreo();
        $datos[$i]["estado"] = (($revisorActual -> getEstado()==0)?"Deshabilitado":(($revisorActual -> getEstado()==1)?"Habilitado":"Inactivo"));
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Revisores", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>