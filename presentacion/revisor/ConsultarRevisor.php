<?php
$revisor = new Revisor();
$cantidad = 5;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}
$revisores = $revisor->consultarPaginacion($cantidad, $pagina);
$totalRegistros = $revisor->consultarCantidad();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
	$totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Revisor</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($revisores) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
				<div class="card-body">
					<table class="table-responsive-lg table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>Cambiar Estado</th>
							<th></th>
						</tr>
						<?php
						$i = 1;
						foreach ($revisores as $revisorActual) {
							echo "<tr>";
							echo "<td>" . $i . "</td>";
							echo "<td>" . $revisorActual->getNombre() . "</td>";
							echo "<td>" . $revisorActual->getApellido() . "</td>";
							echo "<td>" . $revisorActual->getCorreo() . "</td>";
							if ($revisorActual->getEstado() == 1) {
							    echo "<td id='cambioE".$revisorActual->getIdRevisor() ."'>" . 'habilitado' . "</td>";
							    echo "<td><button  data=" . $revisorActual->getIdRevisor()  . " data2=" . $revisorActual->getEstado() . " class='btn btn-xs btn-primary estado'>Cambiar</button></td>";
							} else if ($revisorActual->getEstado() == 0) {
							    echo "<td id='cambioE".$revisorActual->getIdRevisor() ."'>" . 'Inhabilitado' . "</td>";
							    echo "<td><button data=" . $revisorActual->getIdRevisor() . "  data2=" . $revisorActual->getEstado() . " class='btn btn-xs btn-primary estado'>Cambiar</button></td>";
							}
							echo "<td><a href='index.php?pid=" . base64_encode("presentacion/revisor/ModificarRevisor.php") . "&idRevisor=" . $revisorActual->getIdRevisor() . "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a>";
							if($_SESSION["rol"]=="Coordinador")
							{
							     echo "<a href='index.php?pid=" . base64_encode("presentacion/revisor/Crear_Articulo_Revisor.php") . "&idRevisor=" . $revisorActual->getIdRevisor() . "' data-toggle='tooltip' data-placement='left' title='añadir articulo'><span class='fas fa-folder-plus'></span></a></td>";
							}echo "</tr>";
							$i++;
						}
						?>
					</table>
					<div class="text-center">
						<nav  class="table-responsive mb-2">
							<ul class="pagination">
								<li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/revisor/ConsultarRevisor.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
								<?php
								for ($i = 1; $i <= $totalPaginas; $i++) {
									if ($i == $pagina) {
										echo "<li class='page-item active '  ' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
									} else {
									    echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/revisor/ConsultarRevisor.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
									}
								}
								?>
								<li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/revisor/ConsultarRevisor.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
							</ul>
						</nav>
					</div>
					<select id="cantidad">
						<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
						<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
						<option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
						<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#cantidad").on("change", function() {
		url = "index.php?pid=<?php echo base64_encode("presentacion/revisor/ConsultarRevisor.php") ?>&cantidad=" + $(this).val();
		location.replace(url);
	});

	$(document).ready(function(){
		$(".estado").click(function(e) {
			var id = e.target.getAttribute("data");
			var estado = e.target.getAttribute("data2");
			var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/revisor/CambiarEstadoAjax.php") ?>&idRevisor=" + id + "&estado=" + estado;
			$("#cambioE"+id).load(url);
			if(estado == 1){
				e.target.setAttribute("data2","0");
			}else{
				e.target.setAttribute("data2","1");
			}
		});
	});
</script>