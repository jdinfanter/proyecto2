<?php
$revisor = new Revisor();
$revisores = $revisor -> consultarTodos();
$idArticulo = $_GET["idArticulo"];
$articulo = new Articulo($idArticulo,"","","","","","");
$articulo -> consultar();
$fecha="";
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro Revisor</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					    $fecha = date("yy-m-d");
					    $selectRevisor= $_POST["selectRevisor"];
					    $articulo_revisor = new Articulo_Revisor($selectRevisor,$idArticulo,"","",$fecha);
					    $articulo_revisor -> registrar();
					    $articulo -> cambiarEstado($idArticulo,1);
					    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
					    echo 'Articulo añadido con exito';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}
					
					?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/revisor/Crear_Revisor_Articulo.php") ."&idArticulo=".$idArticulo?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Articulo</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $articulo->getNombre() . " ".$revisor ->getApellido();?>" disabled>
						</div>
						<?php 
						    echo "Revisor";
    						echo "<div id='articulo'><select name='selectRevisor' class='form-control'>";
    						//$articulos = $articulo -> consultarSInRevisar();
    						foreach ($revisores as $r) {
    						    echo "<option value='" . $r->getIdRevisor() . "' class='btn btn-outline-secondary'>" . $r->getNombre()." ".$r->getApellido(). "</button>";
    						}
    						echo "</select></div><br>";
						?>
						<button id="btn" type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
