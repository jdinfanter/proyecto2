<?php
$articulo = new Articulo();
$articulos = $articulo -> consultarSInRevisar();
$idRevisor = $_GET["idRevisor"];
$revisor = new Revisor($idRevisor,"","","","","","");
$revisor -> consultar();
$fecha="";
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro Revisor</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					    $fecha = date("yy-m-d");
					    $selectArticulo = $_POST["selectArticulo"];
					    $articulo_revisor = new Articulo_Revisor($idRevisor,$selectArticulo,"","",$fecha);
					    $articulo_revisor -> registrar();
					    $articulo -> cambiarEstado($selectArticulo,1);
					    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
					    echo 'Articulo añadido con exito';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}
					
					?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/revisor/Crear_Articulo_Revisor.php") ."&idRevisor=".$idRevisor?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Revisor</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $revisor ->getNombre() . " ".$revisor ->getApellido();?>" disabled>
						</div>
						<?php 
						    echo "Articulo  ";
    						echo "<div id='articulo'><select name='selectArticulo' class='form-control'>";
    						$articulos = $articulo -> consultarSInRevisar();
    						foreach ($articulos as $a) {
    						    echo "<option value='" . $a->getIdArticulo() . "' class='btn btn-outline-secondary'>" . $a->getNombre() . "</button>";
    						}
    						echo "</select></div><br>";
						?>
						<button id="btn" type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
