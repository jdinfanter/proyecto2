<?php 
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$clave2 = "";
$foto = "";
$fecha="";

if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $clave2 = $_POST["clave2"];
    $fecha = date("yy-m-d");
    $error = "";
    $revisor = new Revisor("",$nombre, $apellido,$correo,$clave,"","");
    if(!$revisor->existeCorreo())
    {
        if($clave==$clave2)
        {
            
            if ($_FILES["imagen"]["name"] != "") {
                $rutaLocal = $_FILES["imagen"]["tmp_name"];
                $tipo_imagen = $_FILES["imagen"]["type"];
                $tiempo = new DateTime();
                $rutaRemota = "Imagen_Revisor/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
                copy($rutaLocal, $rutaRemota);
                $revisor ->consultar();
                if($revisor -> getFoto() != ""){
                    unlink($revisor -> getFoto());
                }
                $revisor = new  Revisor("", $nombre, $apellido, $correo, $clave,$rutaRemota,"");
                $revisor ->registrar();
            } else {
                $revisor ->registrar();
			}
			
			$datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
			$log = new Log("", "Registro Revisor", $datosLog, date("yy-m-d"), date("g:i a"), $nombre." ".$apellido);
			$log -> insertar();
			
			$revisor_Fecha = new Revisor_Fecha("",$fecha,"");
			$id= $revisor_Fecha ->consultarID();
			if($revisor_Fecha -> existeFecha())
			{
			    $cantidad_actual= $revisor_Fechaa ->consultarCantidad();
			    $revisor_Fecha = new Revisor_Fecha($id,$fecha,( $cantidad_actual+1));
			    $revisor_Fecha ->modificar($id);
			}else {
			    $revisor_Fecha = new Revisor_Fecha("",$fecha,1);
			    $revisor_Fecha ->registrar();
			}
			
        }else {
            $error=1;
        }
    }else {
        $error=2;
       
    }
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro Revisor</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					if($error==1)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'La clave no coincide';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else if($error ==2)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'el correo ya existe';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else {
					?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Revisor registrado con exito
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }} ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/revisor/InsertarRevisor.php")?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Clave</label> 
							<input type="password" name="clave" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Confirmar clave</label> 
							<input type="password" name="clave2" class="form-control"  required>
						</div>
						 <div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>