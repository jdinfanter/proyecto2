<?php
if (isset($_POST["modificar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    if($_SESSION["rol"]=="Administrador")
    {
        $estado = $_POST["estado"];
    }
    if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo_imagen = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagen_Revisor/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $revisor = new Revisor($_GET["idRevisor"]);
        $revisor ->consultar();
        if ($revisor ->getFoto() != "") {
            unlink($revisor ->getFoto());
        }
        if($_SESSION["rol"]=="Administrador")
            $revisor  = new Revisor($_GET["idRevisor"], $nombre, $apellido, $correo, "", $rutaRemota, $estado);
            else {
                $estado_Actual= $revisor -> getEstado();
                $revisor = new Revisor($_GET["idRevisor"], $nombre, $apellido, $correo, "", $rutaRemota, $estado_Actual);
            }
            
            $revisor ->editar();
    } else {
        $revisoraux = new Revisor($_GET["idRevisor"]);
        $revisoraux->consultar();
        $foto_Actual =  $revisoraux ->getFoto();
        $estado_Actual=  $revisoraux -> getEstado();
        if($_SESSION["rol"]=="Administrador")
            $revisor = new Revisor($_GET["idRevisor"], $nombre, $apellido, $correo, "", $foto_Actual, $estado);
            else
                $revisor = new Revisor($_GET["idRevisor"], $nombre, $apellido, $correo, "", $foto_Actual, $estado_Actual);
                $revisor->editar();
    }
    
    $userLog = "";
    if ($_SESSION["rol"] == "Revisor")
        $userLog = $nombre . " " . $apellido;
        else
            $userLog = $_SESSION["userName"];
            if($_SESSION["rol"]=="Administrador")
                $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Estado: " . $estado;
                else
                    $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo;
                    $log = new Log("", "Modificar Revisor", $datosLog, date("yy-m-d"), date("g:i a"), $userLog);
                    $log->insertar();
    
} else {
    $revisor = new Revisor($_GET["idRevisor"]);
    $revisor->consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Revisor</h4>
				</div>
				<div class="card-body">
					<?php if (isset($_POST["modificar"])) { ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							Datos editados
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/revisor/ModificarRevisor.php") ?>&idRevisor=<?php echo $_GET["idRevisor"] ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $revisor->getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo  $revisor->getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="email" name="correo" class="form-control" min="1" value="<?php echo  $revisor->getCorreo() ?>" required>
						</div>
						<?php if($_SESSION["rol"]=="Administrador"){?>
						<div class="form-group">
							<label>Estado</label>
							<input type="number" name="estado" class="form-control" min="0" value="<?php echo  $revisor->getEstado() ?>" required>
						</div>
						<?php }?>
						<div class="form-group">
							<label>Imagen</label>
							<input type="file" name="imagen" class="form-control">
						</div>
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>