<?php 
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$clave2 = "";
$foto = "";
$fecha="";
$error="";
if (isset($_POST["registrar"])) {
    
    $select= $_POST["answer"];
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $clave2 = $_POST["clave2"];
    $fecha = date("yy-m-d");
    $error = "";
    if($select==1)
    {   
        $articulista = new Articulista("",$nombre, $apellido,$correo,$clave,"","");
        if(!$articulista ->existeCorreo())
        {
            if($clave==$clave2)
            {
                
                if ($_FILES["imagen"]["name"] != "") {
                    $rutaLocal = $_FILES["imagen"]["tmp_name"];
                    $tipo_imagen = $_FILES["imagen"]["type"];
                    $tiempo = new DateTime();
                    $rutaRemota = "Imagen_Articulista/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
                    copy($rutaLocal, $rutaRemota);
                    $articulista ->consultar();
                    if($articulista -> getFoto() != ""){
                        unlink($articulista -> getFoto());
                    }
                    $articulista = new  Articulista("", $nombre, $apellido, $correo, $clave,$rutaRemota,"");
                    $articulista ->registrar();
                } else {
                    $articulista->registrar();
    			}
    			$datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
    			$log = new Log("", "Registro Articulista", $datosLog, date("yy-m-d"), date("g:i a"), $nombre." ".$apellido);
    			$log -> insertar();
    			
    			$articulista_Fecha = new Articulista_Fecha("",$fecha,"");
    			$id= $articulista_Fecha ->consultarID();
    			if($articulista_Fecha-> existeFecha())
    			{
    			    $cantidad_actual= $articulista_Fecha ->consultarCantidad();
    			    $articulista_Fecha= new Articulista_Fecha($id,$fecha,( $cantidad_actual+1));
    			    $articulista_Fecha ->modificar($id);
    			}else {
    			    $articulista_Fecha  = new Articulista_Fecha("",$fecha,1);
    			    $articulista_Fecha ->registrar();
    			}
    			
            }else {
                $error=1;
            }
        }else {
            $error=2;
           
        }
    }else if($select==2)
    {
        $usuario = new Usuario("",$nombre, $apellido,$correo,$clave,"","");
        if(!$usuario ->existeCorreo())
        {
            if($clave==$clave2)
            {
                
                if ($_FILES["imagen"]["name"] != "") {
                    $rutaLocal = $_FILES["imagen"]["tmp_name"];
                    $tipo_imagen = $_FILES["imagen"]["type"];
                    $tiempo = new DateTime();
                    $rutaRemota = "Imagen_Usuario/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
                    copy($rutaLocal, $rutaRemota);
                    $usuario ->consultar();
                    if($usuario -> getFoto() != ""){
                        unlink($usuario -> getFoto());
                    }
                    $usuario= new  Usuario("", $nombre, $apellido, $correo, $clave,$rutaRemota,"");
                    $usuario ->registrar();
                } else {
                    $usuario->registrar();
                }
                
                $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
                $log = new Log("", "Registro Usuario", $datosLog, date("yy-m-d"), date("g:i a"), $nombre." ".$apellido);
                $log -> insertar();
                
                
                $usuario_Fecha = new Usuario_Fecha("",$fecha,"");
                $id= $usuario_Fecha ->consultarID();
                if($usuario_Fecha -> existeFecha())
                {
                    $cantidad_actual= $usuario_Fecha ->consultarCantidad();
                    $usuario_Fecha= new Usuario_Fecha($id,$fecha,( $cantidad_actual+1));
                    $usuario_Fecha ->modificar($id);
                }else {
                    $usuario_Fecha = new Usuario_Fecha("",$fecha,1);
                    $usuario_Fecha ->registrar();
                }
            }else {
                $error=1;
            }
        }else {
            $error=2;
            
        }
        
    }
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					if($error==1)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'La clave no coincide';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else if($error ==2)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'el correo ya existe';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else {
					?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						 registrado con exito
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }} ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/Registro.php")?>" method="post" enctype="multipart/form-data">
						<div class="form-groupl">
						<label>Registrarse como:</label>
                          <label class="radio">
                            <input type="radio" name="answer" value="1" required>
                            Articulista
                          </label>
                          <label class="radio">
                            <input type="radio" name="answer" value="2">
                            Usuario
                          </label>
                        </div>
                        <div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Clave</label> 
							<input type="password" name="clave" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Confirmar clave</label> 
							<input type="password" name="clave2" class="form-control"  required>
						</div>
						 <div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
					<button name="volver" id="volver" class="btn btn-dark">Volver</button>
            	</div>
            </div>
		</div>
	</div>
</div>
<script>
	$("#volver").click(function(e) {
		url="index.php";
		location.replace(url);
	});
</script>