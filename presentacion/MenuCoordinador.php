<?php
$coordinador = new  Coordinador($_SESSION["id"]);
$coordinador ->consultar();
?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionCoordinador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Articulista
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulista/ConsultarArticulista.php")?>">Consultar Articulista</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Revisor
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/InsertarRevisor.php")?>">Registrar Revisor</a>
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/ConsultarRevisor.php")?>">Consultar Revisor</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Articulos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/ArticulosSinRevisar.php")?>">Articulos sin revisar</a>
        </div>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Coordinador: <?php echo $coordinador -> getNombre() ?> <?php echo $coordinador-> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/ModificarCoordinador.php")?>&idCoordinador=<?php echo $_SESSION["id"]?>" method="post""">Editar Perfil</a> 
						<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/Cambiar_Clave_Coordinador.php")?>&idCoordinador=<?php echo $_SESSION["id"]?>" method="post"">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>