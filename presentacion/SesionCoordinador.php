<?php
$coordinador= new Coordinador($_SESSION["id"]);
$coordinador -> consultar();

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Bienvenido Coordinador</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div id=imagen class="col-3">
              				<img src="<?php echo ($coordinador -> getFoto() != "")?$coordinador -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png"; ?>" width="100%" class="img-thumbnail">
                  			<form action="index.php?pid=<?php echo base64_encode("presentacion/SesionCoordinador.php") ?>" method="post" enctype="multipart/form-data">
    						 </form>           			
              			</div>
              			<div class="col-9">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $coordinador -> getNombre() ?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $coordinador -> getApellido() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $coordinador-> getCorreo() ?></td>
								</tr>
							</table>
						</div>              		
              		</div>              	
            	</div>
            </div>
		</div>
	</div>
</div>

