<?php
$articulista = new  Articulista($_SESSION["id"]);
$articulista->consultar();


?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/SesionArticulista.php")?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
  <ul class="navbar-nav">
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Articulo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulo/CrearArticulo.php")?>">Publicar Articulo</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulo/ConsultarArticulo.php")?>">Mis Articulos</a>
        
        </div>
      </li>
  </ul>


    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Articulista: <?php echo $articulista-> getNombre() ?> <?php echo $articulista-> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulista/ModificarArticulista.php") ?>&idArticulista=<?php echo $_SESSION["id"]?>" method="post">Editar Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulista/Cambiar_Clave_Articulista.php") ?>&idCliente=<?php echo $_SESSION["id"]?>" method="post">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>