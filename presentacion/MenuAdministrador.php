<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<nav class="navbar navbar-expand-xl bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/SesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Articulista
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulista/ConsultarArticulista.php")?>">Consultar Articulista</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Revisor
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/InsertarRevisor.php")?>">Registrar Revisor</a>
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/ConsultarRevisor.php")?>">Consultar Revisor</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Coordinador
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/InsertarCoordinador.php")?>">Registrar Coordinador</a>
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/ConsultarCoordinador.php")?>">Consultar Coordinador</a>
          </div>
      </li>
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/ConsultarUsuario.php")?>">Consultar Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Reportes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" target="_blank" href="reporteArticuloPDF.php">Articulos</a>
          <a class="dropdown-item" target="_blank" href="reporteUsuarioPDF.php">Usuarios</a>
          <a class="dropdown-item" target="_blank" href="reporteArticulistaPDF.php">Articulistas</a>
          <a class="dropdown-item" target="_blank" href="reporteRevisorPDF.php">Revisores</a>
          <a class="dropdown-item" target="_blank" href="reporteCoordinadorPDF.php">Coordinadores</a>
        </div>
      </li>
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estadisticas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulo/Estadistica_Articulo.php")?>">Articulos</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/Estadistica_Usuario.php")?>">Usuarios</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/articulista/Estadistica_Articulista.php")?>">Articulistas</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/revisor/Estadistica_Revisor.php")?>">Revisores</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/Estadistica_Coordinador.php")?>">Coordinadores</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/log/ConsultarLogPagina.php")?>">Log</a>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() ?> <?php echo $administrador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/Editar_administrador.php")?>&idAdministrador=<?php echo $_SESSION["id"]?>">Editar Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/Cambiar_clave.php")?>&idAdministrador=<?php echo $_SESSION["id"]?>" method="post"">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>