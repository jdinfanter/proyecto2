<?php
if (isset($_POST["modificar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    if($_SESSION["rol"]=="Administrador")
    {
        $estado = $_POST["estado"];
    }
    if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo_imagen = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagen_Coordinador/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $coordinador = new Coordinador($_GET["idCoordinador"]);
        $coordinador->consultar();
        if ($coordinador->getFoto() != "") {
            unlink($coordinador->getFoto());
        }
        if($_SESSION["rol"]=="Administrador")
            $coordinador = new Coordinador($_GET["idCoordinador"], $nombre, $apellido, $correo, "", $rutaRemota, $estado);
            else {
                $estado_Actual= $coordinador -> getEstado();
                $coordinador= new Coordinador($_GET["idCoordinador"], $nombre, $apellido, $correo, "", $rutaRemota, $estado_Actual);
            }
            
            $coordinador->editar();
    } else {
        $coordinadoraux = new Coordinador($_GET["idCoordinador"]);
        $coordinadoraux ->consultar();
        $foto_Actual = $coordinadoraux ->getFoto();
        $estado_Actual= $coordinadoraux  -> getEstado();
        if($_SESSION["rol"]=="Administrador")
            $coordinador= new Coordinador($_GET["idCoordinador"], $nombre, $apellido, $correo, "", $foto_Actual, $estado);
            else
                $coordinador= new Coordinador($_GET["idCoordinador"], $nombre, $apellido, $correo, "", $foto_Actual, $estado_Actual);
                $coordinador->editar();
    }
    $userLog = "";
    if ($_SESSION["rol"] == "Coordinador")
        $userLog = $nombre . " " . $apellido;
        else
            $userLog = $_SESSION["userName"];
            if($_SESSION["rol"]=="Administrador")
                $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Estado: " . $estado;
                else
                    $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo;
                    $log = new Log("", "Modificar coordinador", $datosLog, date("yy-m-d"), date("g:i a"), $userLog);
                    $log->insertar();
    
    
} else {
    $coordinador = new Coordinador($_GET["idCoordinador"]);
    $coordinador->consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Coordinador</h4>
				</div>
				<div class="card-body">
					<?php if (isset($_POST["modificar"])) { ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							Datos editados
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/coordinador/ModificarCoordinador.php") ?>&idCoordinador=<?php echo $_GET["idCoordinador"] ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $coordinador->getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo  $coordinador->getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="email" name="correo" class="form-control" min="1" value="<?php echo  $coordinador->getCorreo() ?>" required>
						</div>
						<?php if($_SESSION["rol"]=="Administrador"){?>
						<div class="form-group">
							<label>Estado</label>
							<input type="number" name="estado" class="form-control" min="0" value="<?php echo  $coordinador->getEstado() ?>" required>
						</div>
						<?php }?>
						<div class="form-group">
							<label>Imagen</label>
							<input type="file" name="imagen" class="form-control">
						</div>
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>