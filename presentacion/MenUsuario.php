<?php
$usuario = new Usuario($_SESSION["id"]);
$usuario ->consultar();
?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticulo.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Usuario: <?php echo $usuario -> getNombre() ?> <?php echo $usuario-> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/SesionUsuario.php")?>&idUsuario=<?php echo $_SESSION["id"]?>" method="post""">Ver Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/ModificarUsuario.php")?>&idUsuario=<?php echo $_SESSION["id"]?>" method="post""">Editar Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/Cambiar_Clave_Usuario.php")?>&idUsuario=<?php echo $_SESSION["id"]?>" method="post"">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>