<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$articulista = new Articulista("","","",$correo, $clave);
$revisor = new Revisor("","","",$correo, $clave);
$coordinador = new Coordinador("","","",$correo, $clave);
$usuario = new Usuario("","","",$correo, $clave);
if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $_SESSION["userName"] = $administrador -> getNombre()." ".$administrador -> getApellido();
    header("Location: index.php?pid=" . base64_encode("presentacion/SesionAdministrador.php"));
    $log = new Log("", "Ingreso al sistema", "Rol: Administrador", date("yy-m-d"), date("g:i a"), $administrador -> getNombre()." ".$administrador -> getApellido());
    $log -> insertar();
}else if($articulista -> autenticar()){
    if($articulista -> getEstado() == 0)
    {
        header("Location: index.php?error=2");
    }else {
        $_SESSION["id"] = $articulista -> getIdArticulista();
        $_SESSION["rol"] = "Articulista";
        $_SESSION["userName"] = $articulista -> getNombre()." ".$articulista -> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/SesionArticulista.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Articulista", date("yy-m-d"), date("g:i a"), $articulista  -> getNombre()." ".$articulista  -> getApellido());
        $log -> insertar();
    }
}else if($revisor -> autenticar()){
    if($revisor -> getEstado() == 0)
    {
        header("Location: index.php?error=2");
    }else {
        $_SESSION["id"] = $revisor -> getIdRevisor();
        $_SESSION["rol"] = "Revisor";
        $_SESSION["userName"] = $revisor-> getNombre()." ".$revisor-> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/SesionRevisor.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Revisor", date("yy-m-d"), date("g:i a"), $revisor  -> getNombre()." ".$revisor -> getApellido());
        $log -> insertar();
    }
}else if($coordinador -> autenticar()){
    if($coordinador -> getEstado() == 0)
    {
        header("Location: index.php?error=2");
    }else {
        $_SESSION["id"] = $coordinador -> getIdCoordinador();
        $_SESSION["rol"] = "Coordinador";
        $_SESSION["userName"] = $coordinador-> getNombre()." ".$coordinador-> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/SesionCoordinador.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Coordinador", date("yy-m-d"), date("g:i a"), $coordinador  -> getNombre()." ".$coordinador  -> getApellido());
        $log -> insertar();
    }
}else if($usuario-> autenticar()){
    if($usuario -> getEstado() == 0)
    {
        echo $usuario ->getEstado();
        header("Location: index.php?error=2");
    }else {
        $_SESSION["id"] = $usuario -> getIdUsuario();
        $_SESSION["rol"] = "Usuario";
        $_SESSION["userName"] = $usuario-> getNombre()." ".$usuario-> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/articulo/BuscarArticulo.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Usuario", date("yy-m-d"), date("g:i a"), $usuario -> getNombre()." ".$usuario -> getApellido());
        $log -> insertar();
    }
}else {
    header("Location: index.php?error=1");
}
?>