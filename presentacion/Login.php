

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-4 col-md-3 col-sm-2"></div>
			<div class="col-lg-4 col-md-5 col-sm-7 text-center">
				<div class="card">
				<?php if(isset($_GET["error"]) && $_GET["error"]==1){?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            correo o clave incorrectos.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 </div>
				<?php }else if(isset($_GET["error"]) && $_GET["error"]==2) {?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Su cuenta ha sido inhabilitada
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 </div>
				<?php }?>
    				<div class="card-body">
        				<form class="form-signin" action="index.php?pid=<?php echo base64_encode("presentacion/Autenticar.php") ?>" method="post">
                    		<h1 class="h3 mb-3 font-weight-normal">INICIO</h1>
                			<img src="img/articulo.png" width="220px">

                    		<!--Campo de correo-->
                    		<div class="input-group mb-3">
                    			<div class="input-group-prepend email">
                    				<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
                    			</div>
                    			<input type="email" name="correo" id="inputEmail" class="form-control" placeholder="Correo electronico" required autofocus="" title="Ingrese correo" />
                    		</div>
        
                    		<!--Campo de contrasea-->
                    		<div class="input-group mb-3 contr">
                    			<div class="input-group-prepend pass">
                    				<span class="input-group-text"><i class="fas fa-key"></i></span>
                    			</div>
                    			<input type="password" name="clave" id="inputEmail" class="form-control" placeholder="Contrasea" required autofocus="" title="Ingrese Contrasea" />
                    		</div>
        					<button class="btn btn-lg btn-dark btn-block btn-ingreso" type="submit">
                    			Ingresar
                    		</button>
                    		<p>Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("presentacion/Registro.php")?>">Registrate</a></p>
            			</form>  
        		</div> 
    		</div>  
		</div>
	</div>
</div>