<?php
$articulo = new Articulo();
$cantidad = 5;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}

$articulos = $articulo ->consultarArticulosAprobados($cantidad, $pagina);
$totalRegistros = $articulo->consultarCantidadAprobados();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
	$totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
$mensaje="";
$fecha="";
if (isset($_POST['agregar']))
{   $fecha = date("yy-m-d");
    $comentario = $_POST["comentario"];
    $idArticulo = $_GET["idArticulo"];
    $id= $_GET["idUsuario"];
    $articulo_usuario = new Articulo_Usuario($id,$idArticulo,$comentario,"",$fecha);
    $articulo_usuario  ->registrar();
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
    echo 'Comentario agregado con exito';
    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    echo '</div>';
}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Buscar Articulo</h4>
				</div>
				<div class="card-body">
					<input type="text" id="filtro" class="form-control"
						placeholder="Palabra clave">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container mt-3 " id="resultados">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Articulo</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($articulos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
				<div class="card-body">
					<table class="table-responsive-lg table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th></th>
						</tr>
						<?php
						$i = 1;
						foreach ($articulos as $articuloActual) {
							echo "<tr>";
							echo "<td>" . $i . "</td>";
							echo "<td><a href='index.php?pid=".base64_encode("presentacion/articulo/verArticulo.php")."&idArticulo=".$articuloActual ->getIdArticulo()."'>".$articuloActual ->getNombre()."</a></td>";
							echo "<td>" . $articuloActual->getFecha() . "</td>";
							if($articuloActual->getEstado()==0)
							{
							    $mensaje = "Sin revisar";
							}
							else if($articuloActual->getEstado()==1)
							{
							    $mensaje = "En revision";
							}else if($articuloActual->getEstado()==2)
							{
							    $mensaje = "Aprobado";
							}
							else if($articuloActual->getEstado()==-1)
							{
							    $mensaje = "Pendiente";
							}
							echo "<td><a id='capa".$articuloActual->getIdArticulo()."'>$mensaje</a></td>";
							$ruta= $articuloActual->getArticulo();
							echo "<td><a  target='_blank' href='". $ruta."' data-toggle='tooltip' data-placement='left' title='Ver Articulo'><span class='fas fa-eye'></span></a>";
							echo "<a  target='_blank' href='". $ruta."' data-toggle='modal' data-target='#m" .$articuloActual->getIdArticulo()."' title='Agregar comentario'><span class='fas fa-comment-dots'></span></a>";
							$ruta2="ReporteTrazabilidad.php";
							echo "<a  target='_blank' href='". $ruta2."?idArticulo=".$articuloActual->getIdArticulo()."' data-toggle='tooltip' data-placement='left' title='Ver reporte'><span class='fas fa-file-alt'></span></a></td>";
							echo "</tr>";
							$i++;
							
							
							echo "<div class='modal fade' id='m" . $articuloActual->getIdArticulo() . "' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>" ?>
							<form action="index.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticulo.php") ?>&idUsuario=<?php echo $_SESSION["id"] ?>&idArticulo=<?php echo $articuloActual->getIdArticulo()?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                           <?php echo "<h5 class='modal-title' id='exampleModalLongTitle'>Articulo: " . $articuloActual->getNombre() . "</h5>" ?>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div> 
                                        <div class="modal-body ">
                                           <div class="form-group">
                    							<label>Comentario</label>
                    							<textarea name="comentario" rows="10" cols="40" class="form-control " required ></textarea>
                    						</div>
                                         </div>
                                          <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <input type="submit" name="agregar" style="margin-top:5px;" class="btn btn-primary" value="Añadir comentario" />
                                            </div>
                                     </div>
                                     </form>
                                     
                      <?php echo "</div>"?>
                     <?php } ?>
					</table>
					<div class="text-center">
						<nav  class="table-responsive mb-2">
							<ul class="pagination">
								<li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/articulo/BuscarArticulo.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
								<?php
								for ($i = 1; $i <= $totalPaginas; $i++) {
									if ($i == $pagina) {
										echo "<li class='page-item active '  ' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
									} else {
									    echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/articulo/BuscarArticulo.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
									}
								}
								?>
								<li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/articulo/BuscarArticulo.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
							</ul>
						</nav>
					</div>
					<select id="cantidad">
						<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
						<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
						<option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
						<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticulo.php") ?>&cantidad=" + $(this).val();
	location.replace(url);
});

$(document).ready(function(){
    $("#filtro").keyup(function() {
        if($(this).val().length >= 0){            
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticuloAjax.php") ?>&filtro=" + $(this).val();
    		$("#resultados").load(url);
        }
    });
});
</script>



