<?php
$articulo = new Articulo();
$cantidad = 5;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}
$mensaje="";
$articulos = $articulo -> consultar_Autor($_SESSION["id"],$cantidad, $pagina);
$totalRegistros = $articulo->consultarCantidadArticulos($_SESSION["id"]);
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
	$totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Articulo</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($articulos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
				<div class="card-body">
					<table class="table-responsive-lg table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Estado</th>
							<th></th>
						</tr>
						<?php
						$i = 1;
						foreach ($articulos as $articuloActual) {
							echo "<tr>";
							echo "<td>" . $i . "</td>";
							echo "<td><a href='index.php?pid=".base64_encode("presentacion/articulo/verArticulo.php")."&idArticulo=".$articuloActual ->getIdArticulo()."'>".$articuloActual ->getNombre()."</a></td>";
							echo "<td>" . $articuloActual->getFecha() . "</td>";
							if($articuloActual->getEstado()==0)
							{
							    $mensaje = "Sin revisar";
							}
							else if($articuloActual->getEstado()==1)
							{
							    $mensaje = "En revision";
							}else if($articuloActual->getEstado()==2)
							{
							    $mensaje = "Aprobado";
							}
							else if($articuloActual->getEstado()==-1)
							{
							    $mensaje = "Pendiente";
							}
							echo "<td><a id='capa".$articuloActual->getIdArticulo()."'>$mensaje</a></td>";
							$ruta= $articuloActual->getArticulo();
							echo "<td><a  target='_blank' href='". $ruta."' data-toggle='tooltip' data-placement='left' title='Ver Articulo'><span class='fas fa-eye'></span></a>";
							$ruta2="ReporteTrazabilidad.php";
							echo "<a  target='_blank' href='". $ruta2."?idArticulo=".$articuloActual->getIdArticulo()."' data-toggle='tooltip' data-placement='left' title='Ver reporte'><span class='fas fa-file-alt'></span></a></td>";
							echo "</tr>";
							$i++;
						}
						?>
					</table>
					<div class="text-center">
						<nav  class="table-responsive mb-2">
							<ul class="pagination">
								<li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/articulo/ConsultarArticulo.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
								<?php
								for ($i = 1; $i <= $totalPaginas; $i++) {
									if ($i == $pagina) {
										echo "<li class='page-item active '  ' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
									} else {
									    echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/articulo/ConsultarArticulo.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
									}
								}
								?>
								<li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/articulo/ConsultarArticulo.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
							</ul>
						</nav>
					</div>
					<select id="cantidad">
						<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
						<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
						<option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
						<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#cantidad").on("change", function() {
		url = "index.php?pid=<?php echo base64_encode("presentacion/articulo/ConsultarArticulo.php") ?>&cantidad=" + $(this).val();
		location.replace(url);
	});
</script>