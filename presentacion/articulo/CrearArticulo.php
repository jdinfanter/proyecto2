<?php 
$fecha="";
$nombre = "";
$descripcion = "";
$estado = 0;
$articulista = new Articulista($_SESSION["id"]);

if (isset($_POST["registrar"])) {
    $fecha = date("yy-m-d");
    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $articulista->consultar();
    //$autor = $articulista->getNombre() ." ". $articulista->getApellido();
    $error = "";
    $articulo = new Articulo("",$nombre, $descripcion,$estado,$fecha,"");
    if ($_FILES["articulo"]["name"] != "") {
       $rutaLocal = $_FILES["articulo"]["tmp_name"];
                $tipo_archivo = $_FILES["articulo"]["type"];
                $tiempo = new DateTime();
                $rutaRemota = "Articulos_subidos/". $tiempo->getTimestamp() . (($tipo_archivo == "application/pdf") ? ".pdf" : "");
                copy($rutaLocal, $rutaRemota);
                $articulo -> consultar();
                if($articulo -> getArticulo() != ""){
                    unlink($articulo -> getArticulo());
                }
               
                $articulo = new  Articulo("",$nombre, $descripcion,$estado,$fecha,$rutaRemota);
                $articulo  ->registrar();
            } else {
                $articulo ->registrar();
			}
		$idArt= $articulo -> consultarID();
		$articulo_articulista = new Articulo_Articulista($_SESSION["id"],$idArt);
		$articulo_articulista -> registrar();
		$articulo_Fecha = new Articulo_Fecha("",$fecha,"");
		$id= $articulo_Fecha ->consultarID();
		if($articulo_Fecha -> existeFecha())
		{
		    $cantidad_actual= $articulo_Fecha ->consultarCantidad();
		    $articulo_Fecha = new Articulo_Fecha($id,$fecha,( $cantidad_actual+1));
		    $articulo_Fecha ->modificar($id);
		}else {
		    $articulo_Fecha = new Articulo_Fecha("",$fecha,1);
		    $articulo_Fecha ->registrar();
		}
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Crear Articulo</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					if($error==1)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'La clave no coincide';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else if($error ==2)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'el correo ya existe';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else {
					?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Cliente registrado con exito
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }} ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/articulo/CrearArticulo.php")?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Descripcion</label>
							<textarea name="descripcion" rows="10" cols="40" class="form-control " required ></textarea>
						</div>
						 <div class="form-group">
							<label>Cargar Articulo</label> 
							<input type="file" name="articulo" class="form-control" required >
						</div>
						<button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>