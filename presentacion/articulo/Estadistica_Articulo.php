<?php
$articulo_fecha = new Articulo_Fecha();
$articulos = $articulo_fecha ->consultarTodos();
?>

<script>
    google.charts.load("current", {
        packages: ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ["fecha", "cantidad", {
                role: "style"
            }],
            <?php
            foreach ($articulos as $a){
                echo "['".$a-> getFecha()."',".$a-> getCantidad().",'gold'],";
            }
            ?>

        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2
        ]);

        var options = {
            title: "cantidad de Revisores registrados al día",
            width: 600,
            height: 400,
            bar: {
                groupWidth: "95%"
            },
            legend: {
                position: "none"
            },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("graphics"));
        chart.draw(view, options);
    }
</script>
<center>
<div class="container mt-2">
    <div class="row">
        <div class="col">
            <h3>Estadisticas</h3>
            <h5>Cantidad de Articulos en el día</h5>
            <div class="" id="graphics">
            </div>
        </div>
    </div>
</div>
</center>
