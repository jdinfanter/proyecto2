<?php
$idArticulo = $_GET["idArticulo"];
$articulo = new Articulo($idArticulo,"","","","","");
$articulo->consultar();
$articulo_revisor = new Articulo_Revisor("",$idArticulo,"","","");
$articulos_revisores=$articulo_revisor ->consultarRevisor();
$articulo_usuario = new Articulo_Usuario("",$idArticulo,"","","");
$articulos=$articulo_usuario ->consultarUsuario();
$mensaje="";
$aux=0;
$aux2=0;
?>


<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Informacion del Articulo</h4>
				</div>
				<div class="card-body">
					<form action="index.php?pid=<?php echo base64_encode("presentacion/revisor/ModificarRevisor.php") ?>&idRevisor=<?php echo $_GET["idRevisor"] ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label><strong>Nombre: </strong><?php echo $articulo -> getNombre()?></label>
						</div>
						<div class="form-group">
							<label><strong>Estado: </strong><?php
							if($articulo->getEstado()==0)
							{
							    $mensaje = "Sin revisar";
							}
							else if($articulo->getEstado()==1)
							{
							    $mensaje = "En revision";
							}else if($articulo->getEstado()==2)
							{
							    $mensaje = "Aprobado";
							}
							else if($articulo->getEstado()==-1)
							{
							    $mensaje = "Pendiente";
							}
							echo $mensaje;?></label>
						</div>
						<div class="form-group">
						<label><strong>Descripcion</strong></label>
							<textarea name="descripcion" rows="10" cols="40" class="form-control " disabled><?php echo $articulo -> getDescripcion()?></textarea>
						</div>
						
						<div class="form-group">
							<label><strong>Fecha de subida: </strong><?php echo $articulo -> getFecha()?></label>
						</div>
						<div class="form-group">
							<p><strong>Articulo: </strong><a target="_blank" href="<?php echo $articulo -> getArticulo() ?>"><?php echo $articulo -> getArticulo()?></a></p>
						</div>
						<hr></hr>
							<?php
							foreach ($articulos_revisores as $articuloRevisorActual){
							    if($articuloRevisorActual->getRevisor()=="")
							    {
							        $aux2=0;
							    }else {
							        $aux2=1;
							    }
							}
							if (!$aux2==0)
							{
							   echo ' <div class="form-group">';
							    echo '<label><strong>Revisor: </strong>';
							    echo $articulos_revisores[0] -> getRevisor();
							}else{
							}?></label>
						</div>
						<div class="form-group">
						<?php if(!$aux2==0){?>
							<label><strong>Comentario de <?php echo $articulos_revisores[0] -> getRevisor()?>:</strong></label><br> <?php
							if (!$articulos_revisores[0] -> getObservacion()=="")
							{
							    foreach ($articulos_revisores as $articuloRevisorActual){
							         echo  $articuloRevisorActual ->getObservacion();
							         echo '<br><em><small><p align="right"><strong>comentario realizado la fecha: </strong>'.$articuloRevisorActual ->getFecha().'</small></em></p>';
							     }
							    
							}else{
							    echo "No re registran Comentarios";
							}}?>
						</div>
						<hr></hr>
						<?php 
						foreach ($articulos as $articuloActual){
						    if($articulos[0]->getObservacion()=="")
						    {
						      $aux=0;
						    }else { 
						        $aux=1;
						    }
						}?>
						<?php if (!$aux==0)
						{
						    echo '<p align="center"><strong>Comentarios realizados por los Usuarios</strong></p>';
						    echo ' <hr></hr>';
						}
						    ?>
						<?php foreach ($articulos as $articuloActual)
                        {
                            echo '<div class="form-group">';
                            echo '<label><strong>-comentarios de '.$articuloActual ->getUsuario().":</strong> ".'</label>';
                            echo $articuloActual ->getObservacion();
                            echo '<br><em><small><p align="right"><strong>comentario realizado la fecha: </strong>'.$articuloActual ->getFecha().'</small></em></p>';
                            echo '</div>';
                        }
                        ?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>