<?php
$filtro = $_GET["filtro"];
$articulo= new Articulo();
$articulos = $articulo -> consultarFiltro($filtro);
$mensaje="";
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Articulo</h4>
				</div>
				<div class="text-right"><?php echo count($articulos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($articulos as $articuloActual){
// 						    $posiciones = array();
// 						    for($i=0; $i<strlen($productoActual -> getNombre())-strlen($filtro)+1; $i++){
// 						        if(strtolower(substr($productoActual -> getNombre(), $i, strlen($filtro))) == strtolower($filtro)){
// 						            array_push($posiciones, $i);
// 						        }
// 						    }
						    $pos = stripos($articuloActual -> getNombre(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td><a href='index.php?pid=".base64_encode("presentacion/articulo/verArticulo.php")."&idArticulo=".$articuloActual ->getIdArticulo()."'>".$articuloActual ->getNombre()."</a></td>";
						        
						    }else{						        
						        echo "<td><a href='index.php?pid=".base64_encode("presentacion/articulo/verArticulo.php")."&idArticulo=".$articuloActual ->getIdArticulo()."'>" . substr($articuloActual -> getNombre(), 0, $pos) . "<mark>" . substr($articuloActual -> getNombre(), $pos, strlen($filtro)) . "</mark>" . substr($articuloActual -> getNombre(), $pos+strlen($filtro)) . "</a></td>";
						    }
						    echo "<td>" . $articuloActual -> getFecha() . "</td>";
						    if($articuloActual->getEstado()==0)
						    {
						        $mensaje = "Sin revisar";
						    }
						    else if($articuloActual->getEstado()==1)
						    {
						        $mensaje = "En revision";
						    }else if($articuloActual->getEstado()==2)
						    {
						        $mensaje = "Aprobado";
						    }
						    else if($articuloActual->getEstado()==-1)
						    {
						        $mensaje = "Pendiente";
						    }
						    echo "<td><a id='capa".$articuloActual->getIdArticulo()."'>$mensaje</a></td>";
						    $ruta= $articuloActual->getArticulo();
						    echo "<td><a  target='_blank' href='". $ruta."' data-toggle='tooltip' data-placement='left' title='Ver Articulo'><span class='fas fa-eye'></span></a>";
						    echo "<a  target='_blank' href='". $ruta."' data-toggle='modal' data-target='#m" .$articuloActual->getIdArticulo()."' title='Agregar comentario'><span class='fas fa-comment-dots'></span></a>";
						    $ruta2="ReporteTrazabilidad.php";
						    echo "<a  target='_blank' href='". $ruta2."?idArticulo=".$articuloActual->getIdArticulo()."' data-toggle='tooltip' data-placement='left' title='Ver reporte'><span class='fas fa-file-alt'></span></a></td>";
						    echo "</tr>";
						    $i++;
						    echo "<div class='modal fade' id='m" . $articuloActual->getIdArticulo() . "' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>" ?>
							<form action="index.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticulo.php") ?>&idUsuario=<?php echo $_SESSION["id"] ?>&idArticulo=<?php echo $articuloActual->getIdArticulo()?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                           <?php echo "<h5 class='modal-title' id='exampleModalLongTitle'>Articulo: " . $articuloActual->getNombre() . "</h5>" ?>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div> 
                                        <div class="modal-body ">
                                           <div class="form-group">
                    							<label>Comentario</label>
                    							<textarea name="comentario" rows="10" cols="40" class="form-control " required ></textarea>
                    						</div>
                                         </div>
                                          <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <input type="submit" name="agregar" style="margin-top:5px;" class="btn btn-primary" value="Añadir comentario" />
                                            </div>
                                     </div>
                                     </form>
                                     
                      <?php echo "</div>"?>
                     <?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/articulo/BuscarArticuloAjax.php") ?>&cantidad=" + $(this).val();
	location.replace(url);
});
</script>
